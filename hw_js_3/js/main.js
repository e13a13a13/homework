// 1. Функции в js нам нужны для того, чтобы не повторять один и тот же код в нескольких местах (+ читаемость кода улучшается).
// 2. В функцию передаем параметр(ы)(аргумент(ы)), чтобы можно было потом к нему обратиться внутри самой функции.


let numFirst = +prompt("Enter first number", "0");

let numSecond = +prompt("Enter second number", "0");

let operator = prompt("Enter any operator", "+, -, *, /")

function calcResult(numFirst, numSecond, operator) {
    switch (operator) {
      case '+':
        return numFirst + numSecond;
      case '-':
        return numFirst - numSecond;
      case '*':
        return numFirst * numSecond;
      case '/':
        return numFirst / numSecond;
    }
  }

  console.log(calcResult(numFirst, numSecond, operator));