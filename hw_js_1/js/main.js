// 1. Переменная let - записаное значение в дальнейшем можем менять. Переменная const - значение можем записать только одно, и оно в дольнейшем не меняется. Переменная var - не имеет блочной области видимости, обрабатывается в начале выполнения функции.
// 2. Наверное только потому что это устаревший способ объявления переменных. Рекомендуют этим способом уже не пользоваться.

let userName = prompt("Yuor name?", "Ivan");

console.log(userName);

alert(`Hello ${userName} !`);

let userAge = prompt("Your age?","0");

console.log(userAge);

if(userAge < 18) {
    alert('You are not allowed to visit this website.');
} else if(userAge <= 22) {
    let reQuestioning = confirm('Are you sure you want to continue?');
    if(reQuestioning) {
        alert(`Welcome, ${userName}.`);
    }
    else {
        alert('You are not allowed to visit this website.');
    }
} else {
    alert(`Welcome, ${userName}.`);
}